//
//  ContentView.swift
//  exportDummy
//
//  Created by Angelica Roseanne on 25/10/21.
//


import SwiftUI
import PDFKit


struct ContentView: View {
    var projects = projDetailLog
    var body: some View {
        ScrollView {
            ForEach(projects, id:\.self) {project in
                ProjectWithInfoView(project: project)
            }
        }
        .navigationTitle("test")
        .toolbar {
            ToolbarItem(placement: .primaryAction) {
                Button(action: {
                    print("Export")
                    export(selectedProjects: projects)
                }, label: {
                    Image(systemName: "square.and.arrow.up")
                })
            }
        }
    }
}

func export(selectedProjects: [DummyProjDetails]) {
//    var portfolioViews: [ProjectWithInfoView] = []
    var portfolioViews = [ProjectWithInfoView(project: projDetailLog[0]), ProjectWithInfoView(project: projDetailLog[0])]
    var countY: CGFloat = 0.0
    var imageForViews: [NSImageView] = []
    
//    for proj in selectedProjects {
//        portfolioViews.append(ProjectWithInfoView(project: proj))
//    }
    
    let pdfInfo = NSPDFInfo()
    pdfInfo.url = FileManager.default.urls(for: .documentDirectory,
                                           in: .userDomainMask).first!.appendingPathComponent("Portoflio.pdf")
    pdfInfo.orientation = .landscape
   
    
    for portfolio in portfolioViews {
        let printingView = NSHostingView(rootView: portfolio)
        let contentRect = NSRect(x: 0, y: countY, width: printingView.fittingSize.width, height:  printingView.fittingSize.height)
        countY += printingView.fittingSize.height
        print(countY)
        print(contentRect)
        printingView.frame = contentRect
        
//        printingView.autoresizesSubviews = true
        
        let bitMap = printingView.bitmapImageRepForCachingDisplay(in: contentRect)!
        printingView.cacheDisplay(in: contentRect, to: bitMap)
        

        let image = NSImage(size: bitMap.size)
        image.addRepresentation(bitMap)
        

        let imageView = NSImageView(frame: contentRect)
        imageView.image = image
        
        imageView.bounds = contentRect
        imageForViews.append(imageView)
    }
    
    let pdfPanel = NSPDFPanel()
    pdfPanel.defaultFileName = "Untitled"
    pdfPanel.beginSheet(with: pdfInfo, modalFor: NSApplication.shared.keyWindow!) { result in
        if result == NSApplication.ModalResponse.OK.rawValue {
            let manager = FileManager.default
            let saveURL = pdfInfo.url ?? manager.homeDirectoryForCurrentUser
            
            do {
                var idx = 0
                var pdfDoc = PDFDocument()
                for image in imageForViews {
                    print(image.bounds)
                    
                    if idx == 0 {
                        pdfDoc = PDFDocument(data: image.dataWithPDF(inside: image.bounds))!
                    } else {
                        let pdfPage = PDFPage(image: NSImage(data: image.dataWithPDF(inside: image.bounds))!)
                        pdfDoc.insert(pdfPage!, at: pdfDoc.pageCount)
                        
                    }
                    idx+=1
                }
                pdfDoc.write(to: saveURL)
            }
        }
    }
}

func cobaPrint(portfolio: ContentView) {
    let printInfo = NSPrintInfo()
    printInfo.scalingFactor = 0.5
//    let entryListView = EntryListView(data: ["This is a string", "And another one", "One more"])
    
    let printContainerView = NSHostingView(rootView: portfolio)
    let contentRect = NSRect(x: 0, y: 0, width: 841.8, height:  595.2)
    let bitMap = printContainerView.bitmapImageRepForCachingDisplay(in: contentRect)!
    printContainerView.cacheDisplay(in: contentRect, to: bitMap)


    let image = NSImage(size: bitMap.size)
    image.addRepresentation(bitMap)

    let imageView = NSImageView(frame: contentRect)
    imageView.image = image
    
    
    printContainerView.frame.size = CGSize(width: printContainerView.fittingSize.width, height: printContainerView.fittingSize.height)
    let printOperation = NSPrintOperation(view: printContainerView, printInfo: printInfo)
    printOperation.printInfo.isVerticallyCentered = false
    printOperation.printInfo.isHorizontallyCentered = false
    printOperation.printInfo.orientation = .landscape
    
    imageView.dataWithPDF(inside: imageView.bounds)
    
    printOperation.runModal(for: NSApplication.shared.keyWindow!, delegate: portfolio, didRun: nil, contextInfo: nil)
}
