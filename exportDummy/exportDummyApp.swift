//
//  exportDummyApp.swift
//  exportDummy
//
//  Created by Angelica Roseanne on 25/10/21.
//

import SwiftUI

@main
struct exportDummyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
